version: 4
extractors:
  - name: tap-carbon-intensity
    namespace: carbon
    pip_url: 'git+https://gitlab.com/meltano/tap-carbon-intensity'
  - name: tap-csv
    namespace: tap-csv
    docs: 'http://meltano.com/docs/plugins.html#carbon-intensity'
    pip_url: 'git+https://gitlab.com/meltano/tap-csv.git'
    settings:
      - name: csv_files_definition
        env: TAP_CSV_FILES_DEFINITION
        label: CSV Files Definition
        kind: file
        documentation: https://gitlab.com/meltano/tap-csv#run
    docs: https://meltano.com/docs/plugins.html#tap-csv
  - name: tap-fastly
    namespace: fastly
    docs: 'http://meltano.com/docs/plugins.html#fastly'
    pip_url: 'git+https://gitlab.com/meltano/tap-fastly.git'
    settings:
      - name: api_token
        label: API Token
        kind: password
      - name: start_date
        label: Start Date
        kind: date_iso8601
  - name: tap-gitlab
    namespace: gitlab
    docs: 'http://meltano.com/docs/plugins.html#gitlab'
    pip_url: 'git+https://gitlab.com/meltano/tap-gitlab.git'
    settings_group_validation:
      - ['api_url', 'private_token', 'groups', 'start_date']
      - ['api_url', 'private_token', 'projects', 'start_date']
    settings:
      - name: api_url
        label: GitLab API URL
        value: 'https://gitlab.com/api/v4'
      - name: private_token
        kind: password
        env: GITLAB_API_TOKEN
      - name: groups
        value: ''
        env: GITLAB_API_GROUPS
      - name: projects
        value: ''
        env: GITLAB_API_PROJECTS
      - name: ultimate_license
        value: false
        kind: boolean
        env: GITLAB_API_ULTIMATE_LICENSE
      - name: start_date
        env: GITLAB_API_START_DATE
        kind: date_iso8601
  - name: tap-google-analytics
    namespace: google-analytics
    docs: 'http://meltano.com/docs/plugins.html#google-analytics'
    pip_url: 'git+https://gitlab.com/meltano/tap-google-analytics.git'
    settings:
      - name: key_file_location
        env: GOOGLE_ANALYTICS_API_CLIENT_SECRETS
        kind: file
      - name: view_id
        env: GOOGLE_ANALYTICS_API_VIEW_ID
        label: View ID
      - name: reports
        env: GOOGLE_ANALYTICS_API_REPORTS
      - name: start_date
        env: GOOGLE_ANALYTICS_API_START_DATE
        kind: date_iso8601
      - name: end_date
        env: GOOGLE_ANALYTICS_API_END_DATE
        kind: date_iso8601
  - name: tap-marketo
    namespace: marketo
    docs: 'http://meltano.com/docs/plugins.html#marketo'
    pip_url: 'git+https://gitlab.com/meltano/tap-marketo.git'
    settings:
      - name: identity
      - name: client_id
        label: Client ID
      - name: client_secret
        kind: password
      - name: endpoint
      - name: start_time
        kind: date_iso8601
  - name: tap-mongodb
    namespace: mongodb
    docs: 'http://meltano.com/docs/plugins.html#mongodb'
    pip_url: 'git+https://github.com/singer-io/tap-mongodb.git'
    settings:
      - name: user
        env: MONGODB_USERNAME
      - name: password
        kind: password
      - name: host
      - name: port
        value: 27017
      - name: dbname
        label: Database Name
  - name: tap-salesforce
    namespace: salesforce
    docs: 'http://meltano.com/docs/plugins.html#salesforce'
    pip_url: 'git+https://gitlab.com/meltano/tap-salesforce.git'
    settings:
      # TODO: remove, should be injected from Meltano
      - name: client_id
        label: Client ID
      - name: username
      - name: password
        kind: password
      - name: security_token
        kind: password
      - name: api_type
        value: REST
      - name: select_fields_by_default
        value: true
        kind: boolean
      - name: start_date
        kind: date_iso8601
  - name: tap-stripe
    namespace: stripe
    docs: 'http://meltano.com/docs/plugins.html#stripe'
    pip_url: 'git+https://github.com/meltano/tap-stripe.git@v0.2.4'
    settings:
      - name: account_id
        label: Account ID
      - name: client_secret
        kind: password
        env: STRIPE_API_KEY
      - name: start_date
        kind: date_iso8601
  - name: tap-zendesk
    namespace: zendesk
    docs: 'http://meltano.com/docs/plugins.html#zendesk'
    pip_url: 'tap-zendesk'
    settings:
      - name: email
        kind: email
      - name: api_token
        label: Zendesk API Token
        kind: password
      - name: subdomain
        label: Zendesk Domain
        description: <subdomain>.zendesk.com
      - name: start_date
        kind: date_iso8601
loaders:
  - name: target-csv
    namespace: csv
    docs: http://meltano.com/docs/loaders.html#comma-separated-values-csv
    pip_url: git+https://gitlab.com/meltano/target-csv.git
    settings:
      - name: delimiter
        kind: options
        options:
          - label: Comma (,)
            value: ','
          - label: Tab (  )
            value: '\t'
          - label: Semi-colon (;)
            value: ';'
          - label: Pipes (|)
            value: '|'
        value: ','
        description: A one-character string used to separate fields. It defaults to a comma (,).
      - name: quotechar
        kind: options
        options:
          - label: Single Quote (')
            value: "'"
          - label: Double Quote (")
            value: '"'
        value: "'"
        description: A one-character string used to quote fields containing special characters, such as the delimiter or quotechar, or which contain new-line characters. It defaults to single quote (').
  - name: target-snowflake
    namespace: snowflake
    docs: 'https://www.meltano.com/docs/plugins.html#snowflake'
    pip_url: 'git+https://gitlab.com/meltano/target-snowflake.git'
    signup_url: 'https://trial.snowflake.com?referrer=meltano'
    settings:
      - name: account
        env: SF_ACCOUNT
        description: This is automatically provided by Snowflake and can be found in the URL.
        tooltip: Copy and paste your login URL if you don't know your account ID.
      - name: username
        env: SF_USER
        description: The username you use for logging in
      - name: password
        env: SF_PASSWORD
        kind: password
        description: The password you use for logging in
      - name: role
        env: SF_ROLE
        description: The role that you have assigned privileges for database access
      - name: database
        env: SF_DATABASE
        description: The name of the Snowflake database you want to use
      - name: warehouse
        env: SF_WAREHOUSE
        description: The name of the Snowflake warehouse you want to use
  - name: target-postgres
    namespace: postgres
    docs: 'http://meltano.com/docs/loaders.html#postgresql-database'
    pip_url: 'git+https://github.com/meltano/target-postgres.git'
    settings:
      - name: user
        env: PG_USERNAME
        value: warehouse
      - name: password
        kind: password
        env: PG_PASSWORD
        value: warehouse
      - name: host
        env: PG_ADDRESS
        value: localhost
      - name: port
        env: PG_PORT
        value: 5502
      - name: dbname
        label: Database Name
        env: PG_DATABASE
        value: warehouse
  - name: target-sqlite
    namespace: sqlite
    docs: 'http://meltano.com/docs/loaders.html#sqlite-database'
    pip_url: 'git+https://gitlab.com/meltano/target-sqlite.git'
    settings:
      - name: database
        label: Database
        description: Path to the database, relative to the project root. If the database does not exist yet, it will be created.
        value: warehouse
transformers:
  - name: dbt
    namespace: dbt
    pip_url: dbt
transforms:
  - name: tap-carbon-intensity
    namespace: carbon
    pip_url: 'https://gitlab.com/meltano/dbt-tap-carbon-intensity.git'
    vars:
      entry_table: "{{ env_var('MELTANO_LOAD_SCHEMA') }}.entry"
      generationmix_table: "{{ env_var('MELTANO_LOAD_SCHEMA') }}.generationmix"
      region_table: "{{ env_var('MELTANO_LOAD_SCHEMA') }}.region"
  - name: tap-salesforce
    namespace: salesforce
    pip_url: 'https://gitlab.com/meltano/dbt-tap-salesforce.git'
    vars:
      schema: "{{ env_var('MELTANO_LOAD_SCHEMA') }}"
  - name: tap-gitlab
    namespace: gitlab
    pip_url: 'https://gitlab.com/meltano/dbt-tap-gitlab.git'
    vars:
      schema: "{{ env_var('MELTANO_LOAD_SCHEMA') }}"
      ultimate_license: "{{ env_var('GITLAB_API_ULTIMATE_LICENSE', False) }}"
  - name: tap-stripe
    namespace: stripe
    pip_url: 'https://gitlab.com/meltano/dbt-tap-stripe.git'
    vars:
      livemode: false
      schema: "{{ env_var('MELTANO_LOAD_SCHEMA') }}"
  - name: tap-zendesk
    namespace: zendesk
    pip_url: 'https://gitlab.com/meltano/dbt-tap-zendesk.git'
    vars:
      schema: "{{ env_var('MELTANO_LOAD_SCHEMA') }}"
  - name: tap-google-analytics
    namespace: google-analytics
    pip_url: 'https://gitlab.com/meltano/dbt-tap-google-analytics.git'
    vars:
      schema: "{{ env_var('MELTANO_LOAD_SCHEMA') }}"
models:
  - name: model-carbon-intensity-sqlite
    namespace: carbon
    pip_url: 'git+https://gitlab.com/meltano/model-carbon-intensity-sqlite.git'
  - name: model-carbon-intensity
    namespace: carbon
    pip_url: 'git+https://gitlab.com/meltano/model-carbon-intensity.git'
  - name: model-salesforce
    namespace: salesforce
    pip_url: 'git+https://gitlab.com/meltano/model-salesforce.git'
  - name: model-gitflix
    namespace: gitflix
    pip_url: 'git+https://gitlab.com/jschatz1/model-gitflix.git'
  - name: model-zendesk
    namespace: zendesk
    pip_url: 'git+https://gitlab.com/meltano/model-zendesk.git'
  - name: model-gitlab
    namespace: gitlab
    pip_url: 'git+https://gitlab.com/meltano/model-gitlab.git'
  - name: model-gitlab-ultimate
    namespace: gitlab
    pip_url: 'git+https://gitlab.com/meltano/model-gitlab-ultimate.git'
  - name: model-stripe
    namespace: stripe
    pip_url: 'git+https://gitlab.com/meltano/model-stripe.git'
  - name: model-google-analytics
    namespace: google-analytics
    pip_url: 'git+https://gitlab.com/meltano/model-google-analytics.git'
orchestrators:
  - name: airflow
    namespace: airflow
    pip_url: 'apache-airflow==1.10.2'
    settings:
      - name: core.dags_folder
        value: $MELTANO_PROJECT_ROOT/orchestrate/dags
        env: AIRFLOW__CORE__DAGS_FOLDER
      - name: core.plugins_folder
        value: $MELTANO_PROJECT_ROOT/orchestrate/plugins
        env: AIRFLOW__CORE__DAGS_FOLDER
      - name: core.sql_alchemy_conn
        value: sqlite:///.meltano/orchestrators/airflow/airflow.db
        env: AIRFLOW__CORE__SQL_ALCHEMY_CONN
      - name: core.load_examples
        value: False
        env: AIRFLOW__CORE__LOAD_EXAMPLES
      - name: webserver.web_server_port
        value: 5010
        env: AIRFLOW__WEBSERVER__WEB_SERVER_PORT
